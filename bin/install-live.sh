#!/bin/sh
CONTAINER_HOST=https://comalatech-cheese.herokuapp.com/atlassian-plugin.xml
INSTALL_PATH=/rest/remotable-plugins/latest/installer
USER=$1
CONFLUENCE_HOST=$2

echo container host is $CONTAINER_HOST
echo format is
echo    install-live.sh \<username\> \<confluenceurl\>
echo example
echo    install-live.sh roberto https://corp.comalatech.com

curl -v  -u $USER -X POST -d url=$CONTAINER_HOST $CONFLUENCE_HOST$INSTALL_PATH