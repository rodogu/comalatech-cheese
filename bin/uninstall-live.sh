#!/bin/sh
UNINSTALL_PATH=/rest/remotable-plugins/latest/uninstaller/com.comalatech.cheese
USER=$1
CONFLUENCE_HOST=$2

echo HOST: $CONFLUENCE_HOST
curl -u $USER -v -X DELETE $CONFLUENCE_HOST$UNINSTALL_PATH