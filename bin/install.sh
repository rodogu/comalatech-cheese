#!/bin/sh
DESCRIPTOR=http://localhost:3000/atlassian-plugin.xml
CONFLUENCE_HOST=http://admin:admin@localhost:1990/confluence/rest/remotable-plugins/latest/installer

echo Descriptor: $DESCRIPTOR
echo HOST: $CONFLUENCE_HOST
curl -v -X POST -d url=$DESCRIPTOR $CONFLUENCE_HOST